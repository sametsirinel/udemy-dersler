<?php session_start();

  session_destroy();
  unset($_SESSION["username"]);
  unset($_SESSION["password"]);
  unset($_SESSION["time"]);
  header("Location:index.php");

?>
