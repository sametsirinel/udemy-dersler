<?php 

    class sepet{

        private $renk = 'Sari';

        private $list = [];

        public function boya($renk = 'Sari'){

            $this->renk = $renk;
            return $this;

        }

        public function setList($data=null){

            $this->list = $data;

        }

        public function getList(){

            return $this->list;

        }

        public function yumurtaEkle($sayi = 0){

            $this->list["yumurta"] += $sayi;

        }

        public function sepeteAt($array = []){

            $list = $this->list;

            $list = array_merge($list,$array);

            $this->list = $list;

        }

    }

    $sepet = new sepet();
    $sepet1 = new sepet();
    $sepet->boya("siyah");
    $sepet1->boya("beyaz");

    $sepet1->yumurtaEkle(4);

    $sepet->sepeteAt(["Anahtar"=>2]);
    $sepet1->sepeteAt(["Ayakkabı"=>2]);

    echo "<pre>";
    var_dump($sepet);
    var_dump($sepet1);

?>